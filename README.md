# Qt_Raspberry_Toolchain

Toolchain to build Qt Apps on a host system (like a X86 Desktop) and deploy it on a Raspberry Pi. 
Uses custom Qt and  cross compiler. 

This is a simplified and scripted version of: https://www.kampis-elektroecke.de/raspberry-pi/qt/
Read this if you using a different Raspberry as the Model B 2 or need X11 support.

## Requirements

Raspberry Pi with up-to-date GNU/Linux
SSH Connection to host system

## Install
### Setup Raspberry Pi
```
apt-get build-dep qt5-qmake
apt-get build-dep libqt5gui5
apt-get build-dep libqt5webengine-data
apt-get build-dep libqt5webkit5
apt-get install libudev-dev libinput-dev libts-dev libxcb-xinerama0-dev libxcb-xinerama0 gdbserver

```
Default Qt installation path is: "/usr/local/RaspberryQt"

``` 
mkdir /usr/local/RaspberryQt
chown -R pi:pi /usr/local/RaspberryQt
ln -s /opt/vc/lib/libEGL.so /usr/lib/arm-linux-gnueabihf/libEGL.so.1.0.0 
ln -s /opt/vc/lib/libGLESv2.so /usr/lib/arm-linux-gnueabihf/libGLESv2.so.2.0.0 
ln -s /opt/vc/lib/libEGL.so /opt/vc/lib/libEGL.so.1 
ln -s /opt/vc/lib/libGLESv2.so /opt/vc/lib/libGLESv2.so.2
```
### Setup host system
```
apt install libjasper-dev
apt-get install -y git cmake build-essential pkg-config libjpeg-dev libtiff5-dev libpng-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev libatlas-base-dev gfortran wget unzip libz-dev zlib1g-dev gcc g++ git bison python gperf gdb-multiarch qt5-default texinfo make python3-dev
```
Create new SSH-Key for passwordless remote login
```
ssh-keygen -t rsa -C root@192.168.0.40 -P "" -f ~/.ssh/rpi_root_id_rsa
cat ~/.ssh/rpi_root_id_rsa.pub | ssh root@192.168.0.40 'cat >> .ssh/authorized_keys && chmod 640 .ssh/authorized_keys'
```

create folders
```
mkdir build
mkdir sysroot sysroot/usr sysroot/opt
```

copy sysroot to host
```
rsync -avz -e "ssh -p22 -i ~/.ssh/rpi_root_id_rsa" root@192.168.0.40:/lib sysroot
rsync -avz -e "ssh -p22 -i ~/.ssh/rpi_root_id_rsa" root@192.168.0.40:/usr/include sysroot/usr
rsync -avz -e "ssh -p22 -i ~/.ssh/rpi_root_id_rsa" root@192.168.0.40:/usr/lib sysroot/usr
rsync -avz -e "ssh -p22 -i ~/.ssh/rpi_root_id_rsa" root@192.168.0.40:/opt/vc sysroot/opt
```
run script to resolve requirements and build Qt
```
bash install_host.sh
```
copy Qt to Raspberry Pi
```
rsync -avz -e "ssh -p22 -i ~/.ssh/rpi_root_id_rsa" sysroot/usr/local/RaspberryQt root@192.168.0.40:/usr/local
```
Fix fonts (on Raspberry)
```
mkdir /usr/local/RaspberryQt/lib/fonts
cp /usr/share/fonts/truetype/dejavu/* /usr/local/RaspberryQt/lib/fonts
```

## Demo

open demo/untitled/untitled.pro with Qt Creator on host

For setting up the Qt Creator for remote deployment, see: https://www.kampis-elektroecke.de/raspberry-pi/qt/

