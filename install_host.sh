#! /bin/bash

echo "installing host environment"

git clone https://github.com/raspberrypi/tools
wget https://download.qt.io/archive/qt/5.12/5.12.8/single/qt-everywhere-src-5.12.8.tar.xz
wget https://raw.githubusercontent.com/riscv/riscv-poky/master/scripts/sysroot-relativelinks.py

tar xf qt-everywhere-src-5.12.8.tar.xz
rm qt-everywhere-src-5.12.8.tar.xz

mv qt-everywhere-src-5.12.8 qt_src

cp -R qt_src/qtbase/mkspecs/linux-arm-gnueabi-g++ qt_src/qtbase/mkspecs/linux-arm-gnueabihf-g++

sed -i -e 's/arm-linux-gnueabi-/arm-linux-gnueabihf-/g' qt_src/qtbase/mkspecs/linux-arm-gnueabihf-g++/qmake.conf

python sysroot-relativelinks.py sysroot

TOOLCHAIN_PATH=${PWD}

cd build

../qt_src/configure -device linux-rasp-pi2-g++ -device-option CROSS_COMPILE=${TOOLCHAIN_PATH}/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf- -sysroot ${TOOLCHAIN_PATH}/sysroot -prefix /usr/local/RaspberryQt -opensource -confirm-license -no-gbm -skip qtscript -nomake tests -nomake examples -make libs -pkg-config -no-use-gold-linker -v


#../qt_src/configure -device linux-rasp-pi2-g++ -device-option CROSS_COMPILE=/home/hendrik/git/Qt_Raspberry_Toolchain/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf- -sysroot /home/hendrik/git/Qt_Raspberry_Toolchain/sysroot -prefix /usr/local/RaspberryQt -opensource -confirm-license -no-gbm -skip qtscript -nomake tests -nomake examples -make libs -pkg-config -no-use-gold-linker -v


make -j12
make install
cd ..

