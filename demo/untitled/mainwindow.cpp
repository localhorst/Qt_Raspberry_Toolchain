#include "mainwindow.h"
#include "ui_mainwindow.h"


static uint32_t counter = 0U;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    counter++;
    ui->lcdNumber->display(QString::number(counter));
}
